var express = require("express");
var graphqlHTTP = require("express-graphql");
var { buildSchema } = require("graphql");

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type DemoType {
    investment: String
    commitmentDate: String
    marketValue: Int
  }

  type Query {
    getData: [DemoType]
  }
`);

// The root provides a resolver function for each API endpoint
var root = {
  getData: () => {
    var dummyData = [];
    for (var i = 0; i < 10000; i++) {
      dummyData.push({
        investment: "investment" + i,
        commitmentDate: new Date(
          2019,
          Math.random() * 13,
          Math.random() * 29
        ).toLocaleDateString(),
        marketValue: Math.floor(Math.random() * 100000 + 1)
      });
    }

    return dummyData;
  }
};

var app = express();
app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true
  })
);
app.listen(4000);
