import React from 'react';
import axios from 'axios';
import './App.css';
import { Table } from './components/Table';
import { sortBy } from './utils/sort.util';

export const ColumnHeader = [
  {
    header: 'Investment',
    field: 'investment'
  },
  {
    header: 'Commitment Date',
    field: 'commitmentDate'
  },
  {
    header: 'Market Value',
    field: 'marketValue'
  }
];

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { dataSource: [] };
    this.handleSort = this.handleSort.bind(this);
  }

  componentDidMount() {
    axios
      .post('graphql', {
        query: `{
          getData {
            investment
            commitmentDate
            marketValue
          }
        }`
      })
      .then(res => {
        var result = res.data.data.getData;
        this.setState({ dataSource: result, initDataSource: result });
      });
  }

  handleSort(field, sortOrder) {
    var dataSource = sortBy(this.state.dataSource, field, sortOrder);
    this.setState({ dataSource: dataSource });
  }

  render() {
    return (
      <div className="App">
        <Table
          columnHeader={ColumnHeader}
          dataSource={this.state.dataSource}
          initDataSource={this.state.initDataSource}
          onSortChange={this.handleSort}
          className="TableComponent"
        />
      </div>
    );
  }
}

export default App;
