import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { Table } from './Table';

export const ColumnHeader = [
  {
    header: 'Investment',
    field: 'investment'
  },
  {
    header: 'Commitment Date',
    field: 'commitmentDate'
  },
  {
    header: 'Market Value',
    field: 'marketValue'
  }
];

export const dataSourceTable = [
  {
    investment: 'investment0',
    commitmentDate: '02/02/2019',
    marketValue: 12000
  },
  {
    investment: 'investment1',
    commitmentDate: '02/02/2019',
    marketValue: 12000
  },
  {
    investment: 'investment3',
    commitmentDate: '02/02/2019',
    marketValue: 12000
  },
  {
    investment: 'investment2',
    commitmentDate: '02/02/2019',
    marketValue: 12000
  },
  {
    investment: 'investment2',
    commitmentDate: '02/02/2019',
    marketValue: 12000
  }
];

configure({ adapter: new Adapter() });
describe('TableComponent', () => {
  var wrapper;
  var component;
  beforeEach(() => {
    var onSortChange = jest.fn();
    wrapper = mount(
      <Table
        columnHeader={ColumnHeader}
        dataSource={dataSourceTable}
        initDataSource={dataSourceTable}
        onSortChange={onSortChange}
        className="TableComponent"
      />
    );
    // component = wrapper.instance();
    // wrapper = shallow(<TableComponent />);
    component = wrapper.instance();
  });

  it('should be create component', () => {
    expect(component).toBeDefined();
  });

  it('should be call sortBy', () => {
    var dataSort = {};
    component.setState({ dataSort: dataSort }, () => {
      component.sortBy('investment');
      expect(component.props.onSortChange).toHaveBeenCalledWith('investment', 'desc');
    });

    dataSort['investment'] = 'desc';
    component.setState({ dataSort: dataSort }, () => {
      component.sortBy('investment');
      expect(component.props.onSortChange).toHaveBeenCalledWith('investment', 'asc');
    });

    dataSort['investment'] = 'asc';
    component.setState({ dataSort: dataSort }, () => {
      component.sortBy('investment');
      expect(component.props.onSortChange).toHaveBeenCalledWith('investment', 'desc');
    });

    dataSort['investment'] = 'asc';
    component.setState({ dataSort: dataSort }, () => {
      component.sortBy('');
      expect(component.props.onSortChange).toHaveBeenCalledWith('investment', 'desc');
    });
  });

  it('should be call onDropHeader', () => {
    var item = {
      item: {
        field: 'marketValue'
      }
    };

    component.onDragStartHeader(item);
    component.setState({ itemDrag: 'investment', header: ColumnHeader }, () => {
      component.onDropHeader(item);
    });

    expect(component.state.header[0].field).toBe('marketValue');
  });

  it('should be call array-sort', () => {
    var result = component.array_move(component.props.dataSource, 2, 5);
    console.log(result);
    expect(result[0].investment).toBe('investment0');
  });
});
