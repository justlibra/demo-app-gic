import React from 'react';
import { FixedSizeList as ListRow } from 'react-window';

import './Table.css';

export class Table extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      header: props.columnHeader,
      dataSort: {}
    };
    this.sortBy.bind(this);
    this.onDragStartHeader.bind(this);
    this.onDropHeader.bind(this);
  }

  sortBy(keyField) {
    var typeSort = this.state.dataSort[keyField];
    if (!typeSort || typeSort === 'asc') {
      typeSort = 'desc';
    } else {
      typeSort = 'asc';
    }

    this.props.onSortChange(keyField, typeSort);

    this.setState({
      dataSort: {
        ...this.state.dataSort,
        [keyField]: typeSort
      }
    });
  }

  onDragStartHeader(item) {
    this.setState({ itemDrag: item.field });
  }

  onDropHeader(item) {
    if (this.state.itemDrag && item.field !== this.state.itemDrag) {
      var oldIndex = this.state.header.findIndex(o => o.field === item.field);
      var newIndex = this.state.header.findIndex(o => o.field === this.state.itemDrag);
      var result = this.array_move(this.state.header, oldIndex, newIndex);
      this.setState({ header: result });
    }
  }

  renderHeaders() {
    return this.state.header.map(item => {
      return (
        <th key={item.field}>
          <div
            draggable
            onDragOver={() => this.onDropHeader(item)}
            onDragStart={() => this.onDragStartHeader(item)}
            onClick={() => this.sortBy(item.field)}
          >
            {item.header}
          </div>
        </th>
      );
    });
  }

  array_move(arr, old_index, new_index) {
    if (new_index >= arr.length) {
      var k = new_index - arr.length + 1;
      while (k--) {
        arr.push(undefined);
      }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr;
  }

  renderRows() {
    return (
      this.props.dataSource &&
      this.props.dataSource.map(item => this.state.header.map(header => <td key={header.field}>{item[header.field]}</td>))
    );
  }

  render() {
    var headers = this.renderHeaders();
    var rows = this.renderRows();
    var Row = ({ index, style }) => (
      <td>
        <table>
          <tbody>
            <tr style={style} className="trHeight">
              {rows[index]}
            </tr>
          </tbody>
        </table>
      </td>
    );
    return (
      <table className="tableComponent">
        <thead>
          <tr>{headers}</tr>
        </thead>
        {this.props.dataSource && (
          <ListRow outerTagName="tbody" innerTagName="tr" width={800} height={360} itemSize={35} itemCount={this.props.dataSource.length}>
            {Row}
          </ListRow>
        )}
      </table>
    );
  }
}
