export const sortBy = (arrayCopy, keyField, typeSort) => {
  var result = arrayCopy;
  if (typeSort === 'asc') {
    result = arrayCopy.sort((a, b) => {
      return a[keyField] < b[keyField] ? 1 : a[keyField] > b[keyField] ? -1 : 0;
    });
  } else {
    result = arrayCopy.sort((a, b) => {
      return a[keyField] > b[keyField] ? 1 : a[keyField] < b[keyField] ? -1 : 0;
    });
  }

  return result;
};
