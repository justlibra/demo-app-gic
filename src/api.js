import axios from "axios";

const instance = axios.create({
    baseURL: "/",
    responseType: "json"
});
export default instance;

axios.defaults.baseURL = instance.defaults.baseURL;
axios.defaults.responseType = instance.defaults.responseType;