import React from 'react';
import App from './App';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

jest.mock('axios', () => ({
  post: jest.fn(() => {
    return Promise.resolve({
      data: {
        data: {
          getData: [
            {
              investment: 'investment0',
              commitmentDate: '02/02/2019',
              marketValue: 12000
            }
          ]
        }
      }
    });
  })
}));

configure({ adapter: new Adapter() });

describe('App', () => {
  var wrapper;
  var component;
  beforeEach(() => {
    wrapper = shallow(<App />);
    component = wrapper.instance();
  });

  it('should be create component', () => {
    expect(component).toBeDefined();
  });

  it('should be call handleSort', () => {
    var dataSource = [
      {
        investment: 'investment0',
        commitmentDate: '02/02/2019',
        marketValue: 12000
      },
      {
        investment: 'investment1',
        commitmentDate: '02/02/2019',
        marketValue: 12000
      }
    ];

    component.handleSort(dataSource);
  });
});
